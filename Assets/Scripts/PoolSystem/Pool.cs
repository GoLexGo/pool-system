﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PoolSystem.PoolSystem
{
    public class Pool<T> where T : MonoBehaviour // todo: change to :class
    {
        private T preset;

        private List<T> poolList;
        public List<T> PoolList
        {
            get
            {
                if (poolList != null)
                {
                    return poolList;
                }
                else
                {
                    Debug.LogWarning("PoolList is null\n Create new one");
                    InitPool(preset, parentContainer);
                    return poolList;
                }
            }
        }

        private Transform parentContainer;

        public bool IsEmpty
        {
            get { return poolList.Count == 0; }
        }

        public void InitPool(T preset, Transform parentContainer)
        {
            poolList = new List<T>();
            this.preset = preset;
            PoolMember poolMember = this.preset.gameObject.GetComponent<PoolMember>();
            if (poolMember == null)
            {
                poolMember = this.preset.gameObject.AddComponent<PoolMember>();
            }
            poolMember.ParentContainer = parentContainer;
            this.parentContainer = parentContainer;
        }

        //fetch obj
        public T Pop()
        {
            foreach (var obj in poolList)
            {
                if (obj.transform.parent == parentContainer) // todo: check if gameObject is active
                    //if (!obj.gameObject.activeInHierarchy)
                {
                    obj.gameObject.SetActive(true);
                    return obj;
                }
            }
            // todo: push even if elements are not active - set marginal pool size
            return Push();
        }

        T Push()
        {
            T obj = MonoBehaviour.Instantiate(preset);
            poolList.Add(obj);
            obj.GetComponent<PoolMember>().ParentContainer = this.parentContainer;
            return obj;
        }

        public void SpawnPool( MonoBehaviour poolGameObject, int count)
        {
            poolGameObject.StartCoroutine(SpawnPoolWithDelay(count));
        }

        IEnumerator SpawnPoolWithDelay(int count)
        {
            for (int i = 0; i < count; i++)
            {
                yield return new WaitForEndOfFrame();
                poolList.Add(Spawn());
            }
        }
        #region MonoApiSimplify

        T Spawn()
        {
            T obj = MonoBehaviour.Instantiate(this.preset, parentContainer);
            obj.gameObject.SetActive(false);
            return obj;
        }

        #endregion

    }
}

