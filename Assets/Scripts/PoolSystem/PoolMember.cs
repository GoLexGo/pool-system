﻿using UnityEngine;

namespace PoolSystem.PoolSystem
{
    public class PoolMember : MonoBehaviour
    {
        private void Awake()
        {
            if (ParentContainer == null)
            {
                ParentContainer = UnityPoolManager.unityPoolManager.transform;
            }
        }

        public Transform ParentContainer { get; set; }
     
        public void ReturnToPool()
        {
            transform.SetParent(ParentContainer);
            gameObject.SetActive(false);
        }
    }
}
