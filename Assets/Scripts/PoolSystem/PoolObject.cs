﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Object = UnityEngine.Object;

namespace PoolSystem.PoolSystem
{
    public class PoolObject<T> where T: MonoBehaviour  // todo: change to :class
    {
        /// <summary>
        /// Objects stored in the pool
        /// </summary>
        private Pool<T> pool = null;

        /// <summary>
        /// A function that can be used to override default NewObject( T ) function
        /// </summary>
        public Func<T, T> OnBorn;

        /// <summary>
        /// Actions that can be used to implement extra logic on pushed/popped objects
        /// </summary>
        public Action<T> OnPush, OnPop;

        public PoolObject(Func<T, T> onBorn = null, Action<T> OnPush = null, Action<T> OnPop = null)
        {
            this.OnBorn = onBorn;
            this.OnPush = OnPush;
            this.OnPop = OnPop;
        }

        public PoolObject(T poolObject,Transform parentContainer, Func<T, T> CreateFunction = null, Action<T> OnPush = null, Action<T> OnPop = null)
                                : this(CreateFunction, OnPush, OnPop)
        {
            // Set the poolObject at creation
            pool = new Pool<T>();
            pool.InitPool(poolObject, parentContainer);
        }

        /// <summary>
        ///  SpawnPool the pool with the default poolObject
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public void SpawnPool(MonoBehaviour poolMonoBehaviour, int count)
        {
             pool.SpawnPool(poolMonoBehaviour, count);
        }

        /// <summary>
        /// Fetch an item from the pool
        /// </summary>
        /// <returns></returns>
        public T Pop()
        {
            T objToPop = pool.Pop();

            if (OnPop != null)
                OnPop(objToPop);

            return objToPop;
        }

        /// <summary>
        /// Fetch multiple items at once from the pool
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public T[] Pop(int count)
        {
            if (count <= 0)
                return new T[0];

            T[] result = new T[count];
            for (int i = 0; i < count; i++)
                result[i] = Pop();

            return result;
        }

        /// <summary>
        /// Clear the pool
        /// </summary>
        /// <param name="destroyObjects"></param>
        public void Clear(bool destroyObjects = true)
        {
            //if (destroyObjects)
            //{
            //    // Destroy all the Objects in the pool
            //    foreach (T item in pool)
            //    {
            //        Object.Destroy(item as Object);
            //    }
            //}

            //pool.Clear();
        }

    }
}
