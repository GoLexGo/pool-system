﻿using System.Collections.Generic;
using UnityEngine;

namespace PoolSystem.PoolSystem
{
    public class UnityPoolManager : MonoBehaviour
    {
        private GenericDictionary genericDictionary;
        public static UnityPoolManager unityPoolManager;

        void Awake()
        {
            unityPoolManager = this;
            genericDictionary = new GenericDictionary();
        }
        private Transform GetContainer(string nameContainer)
        {
            return new GameObject(nameContainer + "Container").transform;
        }

        public string RegisterPool<T>( T preset, int poolCapacity) where T : MonoBehaviour
        {
            PoolObject<T> poolObject = new PoolObject<T>(preset, GetContainer(preset.name));
            string keyId = GetKey(preset.name);
            genericDictionary.Add(keyId, poolObject);
            poolObject.SpawnPool(this, poolCapacity);
            poolObject.OnPop = obj => { obj.transform.parent = null; };
            return keyId;
        }

        string GetKey(string nameObj) // todo: change to name + instanceId
        {
            string currentTime = Time.time.ToString();
            return nameObj + currentTime;
        }

        public void GetObj<T>(string id) where T:MonoBehaviour
        {
            genericDictionary.GetValue<PoolObject<T>>(id).Pop();
        }
    }

    public class GenericDictionary
    {
        private Dictionary<string, object> _dict = new Dictionary<string, object>();

        public void Add<T>(string key, T value) where T : class
        {
            _dict.Add(key, value);
        }

        public T GetValue<T>(string key) where T : class
        {
            return _dict[key] as T;
        }
    }
}

