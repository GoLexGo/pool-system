﻿using System.Collections;
using System.Collections.Generic;
using PoolSystem.PoolSystem;
using UnityEngine;

/// <summary>
/// test controller class
/// </summary>
public class TemplateBehaviour : MonoBehaviour
{
    /// <summary>
    /// key of the pool -- todo:// in future release don;t use variable for key - BUT use name of prefab + instanceId of controllerObj
    /// </summary>
    private string keyPreset1;
    private string keyPreset2;

    public TestSpawn prefab1;
    public TestSpawn prefab2;

    public int poolCapacity = 10;

    private void Start()
    {
        keyPreset1 = UnityPoolManager.unityPoolManager.RegisterPool(prefab1, poolCapacity); // register pool for prefab:TestSpawn
        keyPreset2 = UnityPoolManager.unityPoolManager.RegisterPool(prefab2, poolCapacity);
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            SpawnObj(keyPreset1);
        }
        if (Input.GetKey(KeyCode.D))
        {
            SpawnObj(keyPreset2);
        }
    }

    /// <summary>
    /// Spawn prefab1 using pool
    /// </summary>
    private void SpawnObj(string key)
    {
        UnityPoolManager.unityPoolManager.GetObj<TestSpawn>(key);  // call pool manager and get pool using key
    }


}
