﻿using System.Collections;
using System.Collections.Generic;
using PoolSystem.PoolSystem;
using UnityEngine;

/// <summary>
/// Simple spawn obj 
/// </summary>
public class TestSpawn : MonoBehaviour
{
    public float time = 4f;
    private PoolMember poooMember;
    private float speed;
    private MeshRenderer meshRenderer;
    private void Awake()
    {
        poooMember = GetComponent<PoolMember>();
        speed = Random.Range(33,88);
        meshRenderer = GetComponent<MeshRenderer>();
    }

    IEnumerator OnEnableIEnumerator()
    {
        meshRenderer.material.color = Color.green;

        yield return new WaitForSeconds(time);
        meshRenderer.material.color = Color.red;
        //yield return new WaitForEndOfFrame();
        poooMember.ReturnToPool();
    }


    private void Update()
    {
        transform.Rotate(Vector3.back* speed * Time.deltaTime);
    }

    private void OnEnable()
    {
        StartCoroutine(OnEnableIEnumerator());
    }
}
